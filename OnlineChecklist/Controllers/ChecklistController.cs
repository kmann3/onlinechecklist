﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OnlineChecklist.Controllers
{
    public class ChecklistController : Controller
    {
        // GET: Checklist
        public ActionResult Index(string error)
        {
            Models.ChecklistOverviewModel dashboard = new Models.ChecklistOverviewModel();
            dashboard.MyList = Models.Checklist.GetChecklistsForUser(1);
            dashboard.SuboordinateList = Models.Checklist.GetChecklistsAssignedByMe(1);

            if (error != null)
            {
                dashboard.Error = error;
            }
            else
            {
                dashboard.Error = String.Empty;
            }
            return View(dashboard);
        }

        public ActionResult Status(int? checklistId)
        {
            if (checklistId == null)
            {
                return RedirectToAction("Index", new { error = "You've been redirected back here due to an error. There was no ID given to search. If this happens again, you may want to check your bookmarks." });

            }

            if(!checklistId.GetType().Equals(typeof(int)) || checklistId <= 0)
            {
                return RedirectToAction("Index", new { error = "You've been redirected back here due to an error. The ID given was not an integer or was <= 0. If this happens again, you may want to check your bookmarks." });
            }

            var status = Models.Checklist.GetChecklistStatus((int)checklistId);


            return View(status);
        }
    }
}