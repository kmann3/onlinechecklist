﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineChecklist.Models
{
    public class User
    {
        public string Username { get; set; }
        public int UserId { get; set; }
        public string EmailAddress { get; set; }

        public User(int userId)
        {
            Utility.ForceCompileErrorIfNotDebug();
            this.UserId = userId;
            this.Username = "Mann, Kennith";
            this.EmailAddress = "kmann@etherpunk.com";
        }

    }
}
