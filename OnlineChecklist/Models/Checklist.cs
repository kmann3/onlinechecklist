﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineChecklist.Models
{
    public static class Checklist
    {
        /// <summary>
        /// Gets a lists of checklists assigned to me.
        /// </summary>
        /// <param name="userId">UserId self.</param>
        /// <returns>List, duh.</returns>
        /// <remarks>TBI</remarks>
        public static List<Models.ChecklistOverviewModel.Checklist> GetChecklistsForUser(int userId)
        {
            Utility.ForceCompileErrorIfNotDebug();

            List<Models.ChecklistOverviewModel.Checklist> returnList = new List<Models.ChecklistOverviewModel.Checklist>();

            Models.ChecklistOverviewModel.Checklist item1 = new Models.ChecklistOverviewModel.Checklist();
            item1.ChecklistId = 643;
            item1.ChecklistName = "IT Orientation";
            item1.CompletionPercentage = 90;
            item1.DueDate = DateTime.Now.AddMonths(1);
            item1.Notes = "Help intern.";
            item1.OrderId = 3;
            item1.AssignedTo = new User(userId);
            returnList.Add(item1);

            Models.ChecklistOverviewModel.Checklist item2 = new Models.ChecklistOverviewModel.Checklist();

            item2.ChecklistId = 719;
            item2.ChecklistName = "Setup new user";
            item2.CompletionPercentage = 20;
            item2.DueDate = DateTime.Now.AddDays(14);
            item2.Notes = String.Empty;
            item2.OrderId = 5;
            item2.AssignedTo = new User(userId);

            returnList.Add(item2);

            Models.ChecklistOverviewModel.Checklist item3 = new Models.ChecklistOverviewModel.Checklist();
            item3.ChecklistId = 222;
            item3.ChecklistName = "Install new printer";
            item3.CompletionPercentage = 50;
            item3.DueDate = DateTime.Now.AddDays(7);
            item3.Notes = String.Empty;
            item3.OrderId = 7;
            item3.AssignedTo = new User(userId);
            returnList.Add(item3);

            return returnList;
        }

        /// <summary>
        /// Gets a lists of checklists that I have assigned
        /// </summary>
        /// <param name="userId">UserId of person who assigned.</param>
        /// <returns>List, duh.</returns>
        /// <remarks>TBI</remark>
        public static List<Models.ChecklistOverviewModel.Checklist> GetChecklistsAssignedByMe(int userId)
        {
            Utility.ForceCompileErrorIfNotDebug();

            List<Models.ChecklistOverviewModel.Checklist> returnList = new List<Models.ChecklistOverviewModel.Checklist>();

            Models.ChecklistOverviewModel.Checklist item1 = new Models.ChecklistOverviewModel.Checklist();

            item1.ChecklistId = 433;
            item1.ChecklistName = "New Employee Orientation";
            item1.CompletionPercentage = 75;
            item1.DueDate = DateTime.Now.AddMonths(3);
            item1.Notes = String.Empty;
            item1.OrderId=1;
            item1.AssignedTo = new User(userId); // Remember to pull this from the database instead of assuming it's self
            returnList.Add(item1);

            return returnList;
        }

        /// <summary>
        /// Get's a checklist information
        /// </summary>
        /// <param name="checklistId"></param>
        /// <returns></returns>
        public static ChecklistStatusModel GetChecklistStatus(int checklistId)
        {
            Utility.ForceCompileErrorIfNotDebug();

            /* DB Magic here to pull info, probably going to be a View or LINQ query */

            ChecklistStatusModel returnModel = new ChecklistStatusModel();
            returnModel.AssignedUser = new User(1);
            returnModel.ChecklistId = checklistId;
            returnModel.ChecklistName = "Some Checklist Name";
            returnModel.CompletedOn = DateTime.Now.AddDays(-2);
            returnModel.DateDue = DateTime.Now.AddMonths(3);
            List<ChecklistStatusModel.ChecklistItems> checklistItems = new List<ChecklistStatusModel.ChecklistItems>();
            
            /* Random complete times to look a bit more realistic */
            checklistItems.Add(new ChecklistStatusModel.ChecklistItems { AssignedTo = new User(1), CompletedOn = DateTime.Now.AddDays(-2).AddHours(-2).AddMinutes(-30).AddSeconds(-3), ItemId = 433, Notes = String.Empty, TaskName = "TaskNameHere1", TaskSubname = "TaskSubnameHere1", Url = "http://219.271.2.192/index.php" });
            checklistItems.Add(new ChecklistStatusModel.ChecklistItems { AssignedTo = new User(1), CompletedOn = DateTime.Now.AddDays(-3).AddHours(1).AddMinutes(17).AddSeconds(15), ItemId = 713, Notes = "Get with Blah blah about current methods on blah blah.", TaskName = "TaskNameHere1", TaskSubname = "TaskSubnameHere2", Url = String.Empty });
            checklistItems.Add(new ChecklistStatusModel.ChecklistItems { AssignedTo = new User(1), CompletedOn = DateTime.Now.AddDays(-4).AddHours(-4).AddMinutes(45).AddSeconds(33), ItemId = 713, Notes = String.Empty, TaskName = "TaskNameHere2", TaskSubname = "TaskSubnameHere1", Url = String.Empty });

            returnModel.ItemList = checklistItems;

            return returnModel;
        }
    }
}
