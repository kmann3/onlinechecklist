﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineChecklist.Models
{
    public class ChecklistStatusModel
    {
        public DateTime DateDue { get; set; }
        public int ChecklistId { get; set; }
        public string ChecklistName { get; set; }
        public User AssignedUser { get; set; }
        public DateTime? CompletedOn { get; set; }


        public List<ChecklistItems> ItemList { get; set; }

        public class ChecklistItems
        {
            public int ItemId { get; set; }
            public string TaskName { get; set; }
            public string TaskSubname { get; set; }
            public User AssignedTo { get; set; }
            public DateTime? CompletedOn { get; set; }
            public string Url { get; set; }
            public string Notes { get; set; }
        }
    }
}