﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineChecklist.Models
{
    public class ChecklistOverviewModel
    {
        public List<Checklist> MyList;
        public List<Checklist> SuboordinateList;

        public string Error { get; set; }

        public class Checklist
        {
            public int OrderId { get; set; } // Used to sort by
            public int CompletionPercentage { get; set; }
            public string ChecklistName { get; set; }
            public int ChecklistId { get; set; }
            public string Notes { get; set; }
            public DateTime DueDate { get; set; }
            public User AssignedTo { get; set; }
        }
    }
}
