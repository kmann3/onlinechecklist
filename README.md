# README #

### What is this repository for? ###

* This project is meant to manage a checklist with the primary intent of allowing managers to be over your shoulder less. A lot of positions have checklists such as building out a computer, handing out a cell phone, going regular reports, etc. Not all positions allow for linear checklists than be always be done interrupted, so this gives the manager a brief overview of what you have left, what you've done while allowing an employee to follow directions and not have to constantly report or have a manager looming over their head.
* 0.1

### ToDo / Direction ###

This is not even an Alpha release. At the moment this is a wireframe model in the works that does pass fake data. Meaning at some point the Model will pull real data but in the meantime I've hardcoded the parts to return fake data.

### Setup ###

Visual Studio Express (that supports .NET 4.5.2) and SQL Server are the bare minimum requirements. 
Eventually LDAP / AD will be supported somehow or another as an optional method for pulling employees.

Since, currently, fake data is being used -- SQL Server is not needed.  When I get done with the wireframing and plugging things together I'll move on to actually storing the data and place instructions for setting up a server here.

### License ###

This project will be released under the BSD license because it means less drama for me.
What does the BSD license mean? Basically it means you can use it however you want EXCEPT you can't claim you created it. That's it.
Medical? School? Small company? Big company? Government? All of the above can use it without my caring. Only you can't say you created it. Simple, right?
Scroll down for the legal crap that says it in lawyer speak.

### Legal Crap ###

Copyright (c) 2016,  Kennith Mann III
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies,
either expressed or implied, of the FreeBSD Project.